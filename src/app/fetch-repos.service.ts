import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FetchReposService {

  constructor(private http: HttpClient) { }

  fetchRepos(searchText: string): Observable<Array<Repository>> {
    return this.http.get<Array<Repository>>(`https://api.github.com/search/repositories?q=${searchText}`)
			.pipe(
				map((result: any): Array<Repository> => {
					return result.items.splice(0, 5).map((obj: any): Repository => ({
							title: obj.name,
							owner: obj.owner.login,
							created_at: obj.created_at
						})
					);
				})
            );
  }
}

export interface Repository {
  title: string;
  owner: string;
  created_at: string;
}
