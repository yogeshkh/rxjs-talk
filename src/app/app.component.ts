import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FetchReposService } from './fetch-repos.service';

import { switchMap, debounceTime, filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'rxjs-talk';
  repos = [];
  searchTerm: FormControl = new FormControl('');

  constructor(private fetchReposService: FetchReposService) {}

  ngOnInit(): void {
	  this.searchTerm.valueChanges
		.pipe(
			debounceTime(400),
			switchMap((searchText) => this.fetchReposService.fetchRepos(searchText)),
			filter(val => val.length > 0)
		)
		.subscribe((val) => {
				this.repos = val;
	  })


	  
  }
}
